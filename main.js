// Imports

// Server Config
require("dotenv").config();
let path = require("path");
var port = process.env.PORT || 8000;
const express = require("express");
var http = require("http");
const app = express();
const bodyParser = require("body-parser");
const ethers = require("ethers");
const Web3 = require("web3");
const axios = require("axios");
var Promise = require('bluebird');
var cors = require("cors");
app.use(cors()); // Use this after the variable declaration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const web3 = new Web3(Web3.givenProvider || "wss://mainnet.infura.io/ws/v3/975e4dc4fdbe4ea8a81ecba9077932a8");



// Airbrake error reporting
// const Airbrake = require('@airbrake/node');
// new Airbrake.Notifier({
//   projectId: 315054,
//   projectKey: '14c8f5631e4125a622d84e7f52592d1e',
//   environment: process.env.environment
// });



/* WHOP */
const whop = require("./js/sharkey_whop")
// app.post("/discord/whop/create_bot_channel", whop.createBotChannel);
// app.post("/discord/whop/dm", whop.dmUser);
// app.post("/discord/whop/give_role", whop.give_role);
// app.post("/discord/whop/give_prime_role", whop.give_prime_role);
// app.post("/discord/whop/give_proxies_role", whop.give_proxies_role);
// app.post("/discord/whop/create_transaction_channel", whop.createTransactionChannel);
app.post("/discord/whop/create_first_sale_channel", whop.createFirstSaleChannel);
// app.post("/discord/whop/check_server_status", whop.checkServerStatus);
// app.post("/discord/whop/chec k_whopx_server_status", whop.checkWhopxServerStatus);
// app.post("/discord/whop/rental_started", whop.rentalStarted);
// app.post("/discord/whop/rental_ended", whop.rentalEnded);



// app.post("/check_roles_whop", whop.check_roles);
app.get("/whop", function (req, res) {
  // Asteroids
  res.sendFile(path.join(__dirname, "/public/", "whop.html"));
});


app.get("/signature", function (req, res) {

  var nonce = Math.floor(1000 + Math.random() * 9000);
  console.log(req.query)
  console.log(process.env)
  let walletPrivateKey = new ethers.Wallet(
    process.env["R" + req.query.address]
  );
  walletPrivateKey
    .signMessage('Please sign a message to authenticate, nonce: ' + nonce.toString())
    .then((signature) => {
      res.send({
        signature: signature,
        nonce: nonce.toString()
      })
    });

})

app.get("/confirm_address", async function (req, res) {
  const message = req.query.message;
  var signing_address = await web3.eth.accounts.recover(message, req.query.signature)

  res.send({ "address": signing_address })
});



app.post("/confirm_ownership", async function (req, res) {
  try {
    // most likely an erc 721
    let abi = req.body.abi;
    const contract = new web3.eth.Contract(JSON.parse(abi), req.body.contract)
    contract.methods.balanceOf(req.body.address).call(function (err, contractRes) {
      if (contractRes == undefined) return res.send({ "balance": 0 })
      console.log(contractRes);
      res.send({ "balance": contractRes });
    });
  } catch {
    // most likely an erc 1155
    try {
      let abi = req.body.abi;
      const contract = new web3.eth.Contract(JSON.parse(abi), req.body.contract)
      contract.methods.balanceOf(req.body.address, 1).call(function (err, contractRes) {
        if (contractRes == undefined) return res.send({ "balance": 0 })
        res.send({ "balance": contractRes });
      });
    } catch {
      res.send({ "error": "something went wrong" })
    }
  }
});


app.post("/confirm_ownership_two", async function (req, res) {
  // most likely an erc 721
  try {
    if (req.body.standard == "erc721") {
      let abi = req.body.abi;
      const contract = new web3.eth.Contract(JSON.parse(abi), req.body.contract)
      contract.methods.balanceOf(req.body.address).call(function (err, contractRes) {
        if (contractRes == undefined) return res.send({ "balance": 0 })
        res.send({ "balance": contractRes });
      });
    } else if (req.body.standard == "erc1155") {
      try {
        let abi = req.body.abi;
        const contract = new web3.eth.Contract(JSON.parse(abi), req.body.contract)
        contract.methods.balanceOf(req.body.address, req.body.token).call(function (err, contractRes) {
          if (contractRes == undefined) return res.send({ "balance": 0 })
          res.send({ "balance": contractRes });
        });
      } catch (err) {
        console.log(err)
        res.send({ "error": "something went wrong" })
      }
    }
  } catch (error) {
    res.send({ "error": "something went wrong" })
  }
});

app.post("/token_at_index", async function (req, res) {
  try {
    let abi = req.body.abi;
    console.log(req.body)
    const contract = new web3.eth.Contract(JSON.parse(abi), req.body.contract)
    contract.methods.tokenOfOwnerByIndex(req.body.address, req.body.index).call(function (error, token) {
      res.send({ "id": token })
    });

  } catch(e) {
    console.log(e)
    res.send({ "error": "something went wrong" })
  }
});

app.post("/ipfs", async function (req, res) {
  if (req.body.contract == undefined) return res.sendStatus(400)
  if (req.body.token == undefined) return res.sendStatus(400)
  if (req.body.abi == undefined) return res.sendStatus(400)

  try {
    const contract = new web3.eth.Contract(JSON.parse(req.body.abi), req.body.contract)
    contract.methods.tokenURI(req.body.token).call(function (error, token) {
      console.log(error)
      res.send({ "url": token })

    });
  } catch (error) {
    console.log(console.log(error))
    return res.sendStatus(400)
  }
});



http.createServer(app).listen(port, function () {
  console.log("Express server listening on port " + port);
});


process.on("uncaughtException", function (err) {
  console.log(err);
});



