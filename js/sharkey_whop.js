const Discord = require("discord.js");
const config = require("../config/sharkey_whop.json");
require("dotenv").config();
// const Airbrake = require('@airbrake/node');
const utils = require("../utils")
const moment = require("moment")
var airbrake = require('airbrake').createClient("315054", "14c8f5631e4125a622d84e7f52592d1e");
const fs = require('fs');
const PastebinAPI = require('pastebin-js');
const BOT_ID = "815703008075579422"
var schedule = require("node-schedule");


const bots = [
  'wrath aio',     'launcher',         'mercury',
  'anb aio v2',    'rattenfanger',     'easycop',
  'mek aio',       'fleek framework',  'hawkmesh',
  'adept aio',     'kodai',            'nebulabots',
  'tohru',         'eve aio',          'dragon aio',
  'phoenix aio',   'aiomoji',          'galaxsio',
  'dashe',         'scottbotv1',       'project destroyer',
  'phantom',       'polaris',          'sole aio',
  'velox',         's-chrysant',       'rush',
  'sole sneakers', 'cybersole',        'balkobot',
  'mek preme',     'the kick station', 'kilo',
  'phoenixaio',    'kage aio',         'koi',
  'sypion',        'reaio',            'estock',
  'fluid', 'stellar', 'zonos labs', 'viper aio', 'hayha', 'burst aio'
]

const bots_full_names = [
  'Wrath AIO',     'Launcher',         'Mercury',
  'ANB AIO V2',    'Rattenfanger',     'EasyCop',
  'Mek AIO',       'Fleek Framework',  'HawkMesh',
  'Adept AIO',     'Kodai',            'Nebulabots',
  'Tohru',         'Eve AIO',          'Dragon AIO',
  'Phoenix AIO',   'AIOMoji',          'Galaxsio',
  'Dashe',         'Scottbotv1',       'Project Destroyer',
  'Phantom',       'Polaris',          'Sole AIO',
  'Velox',         'S-chrysant',       'Rush',
  'Sole Sneakers', 'Cybersole',        'balkobot',
  'Mek Preme',     'The Kick Station', 'Kilo',
  'PhoenixAIO',    'Kage AIO',         'Koi',
  'Sypion',        'reaio',            'Estock',
  'Fluid', 'Stellar', 'Zonos Labs', 'viper aio', 'Hayha', 'Burst AIO'
]



// new Airbrake.Notifier({
//   projectId: 315054,
//   projectKey: '14c8f5631e4125a622d84e7f52592d1e',
//   environment: process.env.environment
// });


const client = new Discord.Client({
  disableEveryone: true,
  partials: ["MESSAGE", "CHANNEL", "REACTION", "GUILD_MEMBER", "USER"],
  intents: ['GUILD_MEMBERS', 'GUILDS']
});



client.on("guildMemberRemove", function(member){
  discord_account_id = member.id
  if (member.guild.id == "802023806587502613") {
      //whop
    utils.sendRequest(config.domain, `/admin/discord_suspend/${discord_account_id}`, {})
    .then(res => {
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
    })
  } else if (member.guild.id == "817198536802041916") {
    utils.sendRequest(config.domain, `/admin/whopx_status_change/${discord_account_id}/false`, {})
    .then(res => {
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
    })
  }
  
});

client.on("guildMemberAdd", async function(member){
  discord_account_id = member.id
  console.log(`the client joined a guild`);
  if (member.guild.id == "802023806587502613") {
    // Whop
    utils.sendRequest(config.domain, `/admin/discord_unsuspend/${discord_account_id}`, {})
    .then(res => {
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
    })
  } else if (member.guild.id == "817198536802041916") {
    // Whopx
    utils.sendRequest(config.domain, `/admin/whopx_status_change/${discord_account_id}/true`, {})
    .then(res => {
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
    })
    let role = member.guild.roles.fetch("817215648001818656")
    member.roles.add(role)
    .catch(err => err)
  } else if (member.guild.id == "638101979318714398") {
    // Zonos Labs
    checkForActiveZonosRental(member.id)
    .then(async (valid) => {
      if (valid == true) {
        let rentalRoleId = "836694413669433402"
        let role = await member.guild.roles.fetch(rentalRoleId)
        member.roles.add(role)
      }
    })
  } else if (member.guild.id == "735106117218074664") {
    // Zonos Labs
    checkForActiveKoiRental(member.id)
    .then(async (valid) => {
      if (valid == true) {
        let rentalRoleId = "827627000771772456"
        let role = await member.guild.roles.fetch(rentalRoleId)
        member.roles.add(role)
        let secondRole = "857686348802293760"
        role = await member.guild.roles.fetch(secondRole)
        member.roles.add(role)
      }
    })
  }
});

// Fired when a message is reacted to
client.on("messageReactionAdd", async (reaction, user) => {
  
  if (user.id == BOT_ID) return
  let guild = reaction.message.guild
  let channel = reaction.message.channel
  let parent_channel = channel.parentID
  let marketplace_channels_category_id = "816717649639112705"
  let whopx_channel = "835217981708697641"
  let emoji = reaction.emoji.name;
  // Make sure this is a channel from the marketplace category
  if (parent_channel == marketplace_channels_category_id) {
    let user_id = user.id
    // Only allow mods to close them
    let isAdmin = await isAdminOrMod(user_id)
    if (isAdmin == false) return


    switch (emoji) {
      case "✂️": {
        saveTranscript(channel)
        .then(e => {
          channel.delete()
        })
        break;
      }
      
    }
  } else if (channel.id == whopx_channel) {
    // THis is for whopx
    console.log("WHOP X")
    console.log(emoji)
    let roles = guild.roles.cache.array().filter(role => role.name === emoji);
    if (roles.length == 0) {
      airbrake.notify("Couldn't find role with name "+ emoji)
      return
    }
    role = roles[0]
    console.log(role)
    let member = await guild.members.fetch(user.id).catch(console.error)
    member.roles.add(role)
    .then((res) => {
      console.log(`role added to ${user.id}`)
    })
    .catch(console.error)
  }
});

// Fired when a message is reacted to
client.on("messageReactionRemove", async (reaction, user) => {
  
  
  let guild = reaction.message.guild
  let channel = reaction.message.channel
  let whopx_channel = "835217981708697641"
  let emoji = reaction.emoji.name;
  // Make sure this is a channel from the marketplace category
  if (channel.id == whopx_channel) {
    console.log("WHOP X")
    console.log(emoji)
    let roles = guild.roles.cache.array().filter(role => role.name === emoji);
    if (roles.length == 0) {
      airbrake.notify("Couldn't find role with name "+ emoji)
      return
    }
    role = roles[0]
    console.log(role)
    let member = await guild.members.fetch(user.id).catch(console.error)
    member.roles.remove(role)
    .then((res) => {
      console.log(`role removed to ${user.id}`)
    })
    .catch(console.error)
  } 
});



client.on("ready", async (e) => {
    console.log("Logged in to sharkey Whop bot 1")
    client.user.setStatus('available')
    client.user.setPresence({
      status: 'online',
      activity: {
        name: "Adding users, managing roles…",
          type: 'PLAYING'
      }
    })
    
      // sendWhopxMessage("609446928874340423")
    
  // let new_guild = await client.guilds.fetch("883809703308701707")

  // let bot_test_channel = new_guild.channels.resolve("935650632302489650")
  //   const row = new Discord.MessageActionRow()
  //     .addComponents(
  //       new Discord.MessageButton()
  //         .setLabel("Connect Wallet")
  //         .setURL('https://whop.com/products/magicmushroomclubhouse/home')
  //         .setStyle('LINK'),
  //       new Discord.MessageButton()
  //         .setLabel("Support")
  //         .setURL('https://discord.gg/T95jJn8v')
  //         .setStyle('LINK')
  //     );


  // let embed = nftEmbed("Magic Mushroom Clubhouse", "https://lh3.googleusercontent.com/dF_M0RZaCaNU-GG2SGgdmydrf7dyePeps1j45BiURO8wVPBBvTb_L-H5Js62LA657EGE3PUgZLIWTAXNkTZS1imy-QCVMEDlyNz4WA=s130")
    
    // bot_test_channel.send({ embeds: [embed], components: [row]  })
    


    // let members = 0
    // let guilds = client.guilds.cache.array()
    // for (i in guilds) {
    //   members += guilds[i].memberCount
    // }
    // console.log(members)
    // sendWhopxMessage("705971771572224000")
    // const whopguild = await client.guilds.fetch("817198536802041916").catch(e => console.log(e));
    // let members = await whopguild.members.fetch()
    // let i = 0
    // let arr = []
    // members.each(async user => {
    //   for (i in user["_roles"]) {
    //     if (user["_roles"][i] == "817215648001818656") {
    //         // console.log(user.id)
    //         arr.push(user.id)
    //     }
    //   }
    // })
    // console.log("Sending to ", arr.length)
    // arr = arr.filter((v, i, a) => a.indexOf(v) === i);

    // console.log("Sending to ", arr.length)

    // // arr = ["705971771572224000", "705957998580727808"]
    // for (i in arr) {
    //   await sleep(1000)
    //   sendWhopxMessage(arr[i])
    // }


    // sendWhopxMessage("609446928874340423")
    
    // let bot_test_channel = guild.channels.resolve("837397098822500412")
    // bot_test_channel.send({
    //   "content": "This is my channel",
    // })
    

    // bot_support_roles_channel.send("```List #3```\n")

    // var roles_message_id = "835242741617131561"
    // bot_support_roles_channel.messages.fetch(roles_message_id)
    // .then(async message =>  {
    //   message.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
     
    // })




    // var roles_message_id = "836683765128364082"
    // bot_support_roles_channel.messages.fetch(roles_message_id)
    // .then(async message =>  {
    //   message.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
    //   console.log(emojis.length)
    //   for (var i = 4; i < 24; i++) {
    //     await message.react(emojis[i])
    //     .then(e => console.log("reacted ", emojis[i].name))
    //     .catch(e => console.log(" already reacted ", emojis[i].name))
    //   }
    // })

    // var roles_message_id = "836674203100774410"
    // bot_support_roles_channel.messages.fetch(roles_message_id)
    // .then(async message =>  {
    //   console.log(emojis.length)
    //   message.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));


    //   for (var i = 24; i < 34; i++) {
    //     await message.react(emojis[i])
    //     .then(e => console.log("reacted ", emojis[i].name))
    //     .catch(e => console.log(" already reacted ", emojis[i].name))
    //   }
    // })

    // var roles_message_id = "836674215515914271"
    // bot_support_roles_channel.messages.fetch(roles_message_id)
    // .then(async message =>  {
    //   console.log(emojis.length)
    //   message.reactions.removeAll().catch(error => console.error('Failed to clear reactions: ', error));
    //   for (var i = 34; i < emojis.length; i++) {
    //     await message.react(emojis[i])
    //     .then(e => console.log("reacted ", emojis[i].name))
    //     .catch(e => console.log(" already reacted ", emojis[i].name))
    //   }
    // })

    // bot_support_roles_channel.send("j")
 
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

const sendWhopxMessage = async (discord_id) => {
  // const ayy = client.emojis.cache.get("822672910794883102")
  // const ayy = client.emojis.get("822672910794883102");
  let message = `LAST REMINDER\n\n**Sign up for the Whop XYZ Cook Group https://dash.whopxyz.com/purchase and use code promo code "day1fam" to get the first month for only $10**\n\nWe will be giving away **$100** in free Whop Rental Credits tomorrow, don't miss out!\n\nCode will expire Tuesday, 06/08 at 11:59 pm EST!\n\nps: We just added a backdoor provider (TS1 x Fragment :eyes: ) and a SNKRS pass provider.`

  const guild = await client.guilds.fetch("817198536802041916").catch(e => console.log(e));
  await guild.members
  .fetch(discord_id)
  .then(async (user) => {
    user.send(message)
    .then(e => {
      console.log("sent: ", discord_id)
    })
    .catch(e => {
      console.log("error: ", discord_id)
    })
  })
  .catch(e => {
    console.log(e)
  })
}



client.on("message", async (msg) => {
  // return res.send
  try {


    let message = msg["content"];
    let channel_id = msg["channel"].id;
    
    if (channel_id ==  "846831824989978714") {
      utils.sendRequest(config.domain, "/prime/new_release_guide_webhook", {"embeds": msg.embeds})
      return
    }


    let command  = process.env.WHOP_BOT_COMMAND
    if (message.substring(0, command.length).toLowerCase() == command) {
      var args = message.substring(command.length + 1).split(" ");
      var cmd = args[0];
      switch (cmd) {
        case "add-transaction-feed":
          return bindChannel(msg, 'Whop Sales Feed', "transaction_webhook");
        case "add-rental-feed":
          return bindChannel(msg, "Whop Listings Feed", "new_listing_webhook");
        case "help":
          return helpBotsWebhook(msg)
        case "help-server":
          return helpWebhook(msg)
        case "setup":
          return setupServer(msg)
        case "transcript":
          return transcript(msg)
        case "airdrop":
          return airdrop(msg, args)
        case "proxy":
          return airdropProxies(msg, args, "proxies")
        case "accounts":
          return airdropProxies(msg, args, "accounts")
        case "captcha":
          return airdropProxies(msg, args, "captcha")
        case "weeklyisp":
          return airdropProxies(msg, args, "weeklyisp")
        case "monthlyisp":
          return airdropProxies(msg, args, "monthlyisp")
        case "new-bot":
            return newBot(msg, args)
        case "zonos-invite":
          return requestZonos(msg)
        case "koi-invite":
          return requestKoi(msg)
        case "hayha-invite":
          return requestHayha(msg)
        case "affiliate":
          return makeAffiliate(msg, args)
        case "reset":
          return resetKey(msg, args[1])
        case "dashboard":
          return msg.reply("https://whop.io/hub")
        case "sms":
          // return sendSmsBlast(msg, args);
        case "push":
          return sendPushNotification(msg, args)
        default:
          return getBotChart(msg, message.substring(command.length + 1))
      }
    }
  } catch {

  }
});

const sendPushNotification = (msg, terms) => {
  let discord_account_id = msg.author.id;
  let body = ""
  let url = ""
  for (i in terms) {
    if (i >= 1) {
      if (terms[i].includes("http")) {
        url = terms[i]
      } else {
        body = body + terms[i] + " "
      }
    }
  }
  
  let auth = process.env["AIRDROP_PASSWORD"]

  utils.sendRequest("https://business.whop.com", `/discord/send_push_notification`, { discord_account_id, body, url, auth })
  .then(body => {
    return msg.reply(body["message"]);
  }).catch(err => {
    console.log(err)
    airbrake.notify(err)
  })
}



const sendSmsBlast = async (msg, terms) => {
  // Check the user permissions
  let discord_account_id = msg.author.id;
  let i = 0
  let str = ""
  for (i in terms) {
    if (i >= 1) {
      str = str + terms[i] + " "
    }
  }
  utils.sendRequest(config.domain, `/discord/sms_blast`, {discord_account_id, str})
  .then(body => {
    console.log(body)
    if (body["success"] == true && !body["message"]) {
      return msg.reply(`This SMS blast has been queued up.`);
    } else {
      return msg.reply(body["message"]);
    }
  }).catch(err => {
    console.log(err)
    airbrake.notify(err)
  })
}




const resetKey = async (msg, key) => {
  // Check the user permissions
  let discord_account_id = msg.author.id;
  utils.sendRequest(config.domain, `/discord/reset_key`, {discord_account_id, key})
  .then(body => {
    console.log(body)
    if (body["success"] == true && !body["message"]) {
      return msg.reply(`This key has been reset.`);
    } else {
      return msg.reply(body["message"]);
    }
  }).catch(err => {
    console.log(err)
    airbrake.notify(err)
  })
}



const requestZonos = (msg) => {
  let discord_account_id = msg.author.id
  checkForActiveZonosRental(discord_account_id)
  .then(async valid => {
    console.log(valid)
    if (valid == true) {
      const welcomeChannel = await client.channels.fetch("638105985298202625").catch(e => console.log(e));
      let invite = await welcomeChannel.createInvite({
        maxUses: 1,
        maxAge: 86400,
        unique: true,
        reason: "Whop bot"
      }).catch(console.error);

      if (invite) {
        let url = `https://discord.gg/${invite.code}`
        let message = `Hi! Your recent rental of **Zonos Labs** also comes with private Discord access!\n\nJoin their server here: ${url}`
        msg.reply(message)
      } else {
        msg.reply("Something went wrong :worried:")
      }
    } else {
      msg.reply("You don't have an active Zonos rental :worried:")
    }
  })

}

const requestHayha = (msg) => {
  let discord_account_id = msg.author.id
  checkForActiveHayhaRental(discord_account_id)
  .then(async valid => {
    console.log(valid)
    if (valid == true) {
      const welcomeChannel = await client.channels.fetch("789196329921806346").catch(e => console.log(e));
      let invite = await welcomeChannel.createInvite({
        maxUses: 1,
        maxAge: 86400,
        unique: true,
        reason: "Whop bot"
      }).catch(console.error);

      if (invite) {
        let url = `https://discord.gg/${invite.code}`
        let message = `Hi! Your recent rental of **Hayha** also comes with private Discord access!\n\nJoin their server here: ${url}`
        msg.reply(message)
      } else {
        msg.reply("Something went wrong :worried:")
      }
    } else {
      msg.reply("You don't have an active Koi rental :worried:")
    }
  })

}

const requestKoi = (msg) => {
  let discord_account_id = msg.author.id
  checkForActiveKoiRental(discord_account_id)
  .then(async valid => {
    console.log(valid)
    if (valid == true) {
      const welcomeChannel = await client.channels.fetch("735253597180264468").catch(e => console.log(e));
      let invite = await welcomeChannel.createInvite({
        maxUses: 1,
        maxAge: 86400,
        unique: true,
        reason: "Whop bot"
      }).catch(console.error);

      if (invite) {
        let url = `https://discord.gg/${invite.code}`
        let message = `Hi! Your recent rental of **Koi** also comes with private Discord access!\n\nJoin their server here: ${url}`
        msg.reply(message)
      } else {
        msg.reply("Something went wrong :worried:")
      }
    } else {
      msg.reply("You don't have an active Koi rental :worried:")
    }
  })

}

const newBot = async (msg, args) => {
  let discord_account_id = args[1]
  let amount = args[2]
  // Check the user permissions
  let userId = msg.author.id;
  const guild = await client.guilds.fetch("817198536802041916").catch(e => console.log(e));
  await guild.members
    .fetch(userId)
    .then(async (user) => {
      let admin_role_id = ""
      for (i in user["_roles"]) {
        await guild.roles.fetch(user["_roles"][i]).then(async (role) => {
          if (role.id == "817204387133915207") {
            const filter = (m) => m.author.id === msg.author.id;
            msg.reply("Enter the bot name")
            await msg.channel
            .awaitMessages(filter, {
              max: 1,
              time: 240000,
            })
            .then(async (collected) => {
              let message = collected.first().content;
              if (message.trim() == "cancel") return
              let bot_name = message.trim()
              msg.reply("Enter the bot image URL")
              await msg.channel
              .awaitMessages(filter, {
                max: 1,
                time: 240000,
              })
              .then(async (collected) => {
                let message = collected.first().content;
                if (message.trim() == "cancel") return
                
                createBotChannelCore(bot_name, image_url)
                .then(e => {
                  msg.reply("Done. Make sure to thank you to my lord and savior Sharkey")
                })
                
              })
              .catch(err => {
                console.log(err)
                return msg.reply("Please try again later")
              })

              
            })
            .catch(err => {
              console.log(err)
              return msg.reply("Please try again later")
            })
                    
          }
        });
      }
    })
    .catch((err) => {
      console.log("err");
      console.log(err);
    });
}


const makeAffiliate = async (msg, args) => {
  let email = args[1]
  console.log("hi")
  // Check the user permissions
  let userId = msg.author.id;
  const guild = await client.guilds.fetch("802023806587502613").catch(e => console.log(e));
  await guild.members
    .fetch(userId)
    .then(async (user) => {
      let admin_role_id = ""
      for (i in user["_roles"]) {
        await guild.roles.fetch(user["_roles"][i]).then((role) => {
          if (role.id == "806311893940961290") {
            let info = {
              "auth": process.env["AIRDROP_PASSWORD"],
              "email": email
            }

            utils.sendRequest(config.domain, `/discord/make_affiliate`, info)
            .then(body => {
              console.log(body)
              if (body["success"] == "true" || body["success"] == true) {
                return msg.reply(`They are now an affiliate.`);
              } else {
                return msg.reply(body["message"]);
            }
            }).catch(err => {
              console.log(err)
              airbrake.notify(err)
            })
                    
                    
          }
        });
      }
    })

    .catch((err) => {
      console.log("err");
      console.log(err);
    });

}

const airdrop = async (msg, args) => {
  console.log("hi")
  let discord_account_id = args[1]
  let amount = args[2]
  // Check the user permissions
  let userId = msg.author.id;
  const guild = await client.guilds.fetch("802023806587502613").catch(e => console.log(e));
  await guild.members
    .fetch(userId)
    .then(async (user) => {
      let admin_role_id = ""
      for (i in user["_roles"]) {
        await guild.roles.fetch(user["_roles"][i]).then((role) => {
          if (role.id == "806311893940961290") {
            let info = {
              "auth": process.env["AIRDROP_PASSWORD"],
              "discord_account_id": discord_account_id,
              "amount": amount
            }
            
            utils.sendRequest(config.domain, `/discord/airdrop`, info)
            .then(body => {
              console.log(body)
              if (body["success"] == "true" || body["success"] == true) {
                return msg.reply(`Airdrop coming in hot for ${discord_account_id} :exploding_head: :fire: `);
              } else {
                return msg.reply(body["message"]);
            }
            }).catch(err => {
              console.log(err)
              airbrake.notify(err)
            })
                    
                    
          }
        });
      }
    })

    .catch((err) => {
      console.log("err");
      console.log(err);
    });

}
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const airdropProxies = async (msg, args, resource) => {
  let discord_account_id = args[1]
  let amount = args[2]
  // Check the user permissions
  let userId = msg.author.id;
  const guild = await client.guilds.fetch("802023806587502613").catch(e => console.log(e));
  await guild.members
    .fetch(userId)
    .then(async (user) => {
      let admin_role_id = ""
      for (i in user["_roles"]) {
        await guild.roles.fetch(user["_roles"][i]).then((role) => {
          if (role.id == "806311893940961290") {
            let info = {
              "auth": process.env["AIRDROP_PASSWORD"],
              "discord_account_id": discord_account_id,
              "amount": amount,
              "resource": resource
            }
            
            if (resource == "proxies") {
              url = "/discord/airdrop_proxies"
            } else {
              url = "/discord/airdrop_accounts"
            }
            utils.sendRequest(config.domain, url, info)
            .then(body => {
              console.log(body)
              if (body["success"] == "true" || body["success"] == true) {
                return msg.reply(`${capitalizeFirstLetter(resource)} coming in hot for ${discord_account_id} :exploding_head: :fire: `);
              } else {
                return msg.reply(body["message"]);
            }
            }).catch(err => {
              console.log(err)
              airbrake.notify(err)
            })
                    
                    
          }
        });
      }
    })

    .catch((err) => {
      console.log("err");
      console.log(err);
    });

}

const saveTranscript = async (channel) => {
  return new Promise(async function (resolve, reject) {
    channel.messages.fetch()
    .then(messages => {
        let text = "";

        for (let [key, value] of messages) {
            const date = new Date(value.createdTimestamp);
            let dateString = `${date.getDate()}/${date.getMonth()} ${date.getHours()}h ${date.getMinutes()}m`;
            text += `${value.author.tag} at ${dateString}: ${value.content}\n`;
        }

        pastebin.createPaste({
                text: text,
                title: channel.name,
                format: null,
                privacy: 1
            })
            .then(async data => {
                const transcriptChannel = await client.channels.fetch("818872848185032724").catch(e => console.log(e));

                transcriptChannel.send(`Transcript ${channel.name} for: ${data}`)
                    .then(() => {
                      return resolve()
                    })
                    .catch((err) => {
                        console.log(`Could not PM transcript, falling back to message in channel: ${err}`);
                        transcriptChannel.send(data).fail((err) => console.log(`Uh oh! Something went wrong: ${err}`));
                        return reject()
                    });
            })
            .fail(err => {
                console.log(`Failed to create paste: ${err}`);
                return reject()
            });
    })
    .catch(err => {
        console.log(`Failed to fetch messages: ${err}`);
        return reject()
    });
  });
}

const setupServer = async (msg) => {
  let member = msg.member
  if (!member) {
    let errorMsg = createErrorEmbed("Uh oh", "This command must be written in the channel you want the feed to be in.")
    return msg.reply(errorMsg)
  }
  if (!member.hasPermission('MANAGE_CHANNELS')) {
    return msg.reply("You must have the `Manage Channels` permission to use this command.")
  }
  let reaction = await msg.react("⏰")
  let guild = msg.guild
  let channels = guild.channels
  let info = utils.getInfoFromMessage(msg)
  const everyone = guild.roles.everyone;
  let category = await channels.create("Whop.io Rentals", {type: "category" , permissionOverwrites: [
    {
      id: everyone.id,
      deny: 'SEND_MESSAGES',
    }
  ]}).catch(err => {
    console.log(err)
    msg.react("⚠️")
    return msg.reply("I was unable to create the channels in your server. Please make sure the `Moderation` settings for this server are set to `None`. https://cdn.discordapp.com/attachments/709856186463158312/830611830468378644/unknown.png") 
  })
  if (undefined == category) return


  let new_listings = await channels.create("New Listings", {parent: category, permissionOverwrites: [
    {
      id: everyone.id,
      deny: 'SEND_MESSAGES'
    }
  ]}).catch(err => {
    console.log(err)
    msg.react("⚠️")
    return msg.reply("I was unable to create the channels in your server. Please make sure the `Moderation` settings for this server are set to `None`. https://cdn.discordapp.com/attachments/709856186463158312/830611830468378644/unknown.png") 
  })

  if (undefined == new_listings) return


  new_listings_webhook =  await new_listings.createWebhook("New Rental Listing", {
    avatar: 'https://cdn.discordapp.com/attachments/709856186463158312/816131765936324678/7d6c62cac833c8293aec9a4ef850ba58_copy.png',
  })

  info["new_listing_webhook_url"] = new_listings_webhook.url
  info["new_listing_channel_id"] = new_listings.id

  let recent_sales = await channels.create("Recent Sales", {parent: category, permissionOverwrites: [
    {
      id: everyone.id,
      deny: 'SEND_MESSAGES'
    }
  ]}).catch(err => {
    console.log(err)
    msg.react("⚠️")
    return msg.reply("I was unable to create the channels in your server. Please make sure the `Moderation` settings for this server are set to `None`. https://cdn.discordapp.com/attachments/709856186463158312/830611830468378644/unknown.png") 
  })

  if (undefined == recent_sales) return



  recent_sales_webhook = await recent_sales.createWebhook("New Rental Transaction", {
    avatar: 'https://cdn.discordapp.com/attachments/709856186463158312/816131765936324678/7d6c62cac833c8293aec9a4ef850ba58_copy.png',
  })

  info["transaction_webhook_url"] = recent_sales_webhook.url
  info["transaction_channel_id"] = recent_sales.id
  console.log(info)
  utils.sendRequest(config.domain, `/discord_servers/set_webhook`, info)
  .then(res => {
    reaction.remove()
    msg.react('✅');
    sendSuccessWebhook(recent_sales_webhook, "New Whop transactions will now be sent to this channel.")
    sendSuccessWebhook(new_listings_webhook, "New Whop listings will now be sent to this channel.")
    msg.reply(`Success! You now have a direct feed in <#${new_listings.id}> and <#${recent_sales.id}>`)
  }).catch(err => {
    console.log(err)
    airbrake.notify(err)
    return sendErrorWebhook(webhook,"Something went wrong :worried:", "Rest assured, our team is working on it. Please try again later.")
  })


}

const buildSellerChannelEmbed = (seller_id, middleman_id, bot_name) => {
  const embed = new Discord.MessageEmbed()
  .setTitle(`Congrats! Your ${bot_name} copy just sold!`)
  .setDescription(`Congrats <@${seller_id}>! You sold your ${bot_name} copy on Whop. The middleman process is now underway. The next step is binding your key to your Whop middleman <@${middleman_id}>. We will use this chat to facilitate that process. As soon as you are ready, please send the key over and tag the middleman in this chat. DO NOT UNBIND THE KEY YET, just send over the license key. \n\nWithin 24 hours of your key being bound to the Whop dashboard, we will collect the funds from your buyer.\n\nIf you have any questions about the middleman process, you can learn more [here](http://docs.whop.io/)`)
  .setFooter("Whop.io","https://cdn.discordapp.com/attachments/709856186463158312/816131765936324678/7d6c62cac833c8293aec9a4ef850ba58_copy.png", "https://whop.io")
  .setColor("#ff5754")
  .setTimestamp()
  return embed
}

const buildBuyerChannelEmbed = (buyer_id, middleman_id, bot_name) => {
  const embed = new Discord.MessageEmbed()
  .setTitle(`Congrats on your new ${bot_name} purchase!`)
  .setDescription(`<@${buyer_id}>, You're soon to be the proud new owner of a ${bot_name} copy! The middleman process is now underway. As soon as your seller binds their key to the Whop dashboard, you will be prompted to complete your payment for the bot. If your payment is not processed within 24 hours of the key being bound to Whop, your sale will be automatically canceled.\n\nOnce your payment process, we will send over all the key ownership information, giving you access to bind it to your own account. \n\nIf you have any questions about the middleman processes, you can learn more [here](http://docs.whop.io/). <@${middleman_id}>`)
  .setFooter("Whop.io","https://cdn.discordapp.com/attachments/709856186463158312/816131765936324678/7d6c62cac833c8293aec9a4ef850ba58_copy.png", "https://whop.io")
  .setColor("#ff5754")
  .setTimestamp()
  return embed
}

// const nftEmbed = (bot_name, iconURL) => {
//   const embed = new Discord.MessageEmbed()
//     .setAuthor({ name: bot_name + " Holders", iconURL: "https://media.discordapp.net/attachments/882668144508944384/934174758394806302/ColorRed.png?width=1133&height=525"})
//     .setTitle("Verify your assets")
//     .setDescription(`Here you'll be able to verify ownership of your ${bot_name} NFT.\n\n This is a read - only connection. Do not share your private keys. We will never ask for your seed phrase.`)
//     .setColor("#58b9ff")
//     .setThumbnail(iconURL)
//   return embed
// }



const buildBotEmbed = (data) => {
  const embed = new Discord.MessageEmbed()
  .setAuthor(data["title"], data["image_url"])
  .addField("Lowest Daily Ask", data["lowest_daily_ask"], true)
  .addField("Lowest Weekly Ask", data["lowest_weekly_ask"], true)
  .addField("Lowest Monthly Ask", data["lowest_monthly_ask"], true)
  .addField("Weekly Sales", data["weekly_sales"], true)
  .addField("Monthly Sales", data["monthly_sales"], true)
  .addField("Lifetime Sales", data["lifetime_sales"], true)
  .addField("In Stock Today", data["in_stock_today"], true)
  .addField("Active Rentals", data["active_rentals"], true)
  .addField("Total Keys", data["total_keys"], true)
  .addField("Rent Here", data["whop_url"], false)
  .addField("Join Whop", "https://discord.gg/ZTxHxtuXTM", false)
  .setFooter("Whop.io","https://cdn.discordapp.com/attachments/709856186463158312/816131765936324678/7d6c62cac833c8293aec9a4ef850ba58_copy.png", "https://whop.io")
  .setImage(data["chart_url"])
  .setColor("#ff5754")
  .setTimestamp()
  return embed
}

const getBotChart = async (msg, bot_name) => {
  let channel = msg.channel;
  bot_name = bot_name.toLowerCase()
  if (!bots.includes(bot_name)) return
  const user = await client.users
  .fetch(msg.author.id)
  .catch((err) => console.log(err));
  msg.react('✅');

  utils.sendRequest(config.domain, `/discord_servers/get_bot_info`, {bot_name})
  .then(res => {
    let embed = buildBotEmbed(res)
    if (channel) {
      channel.send(embed);
    } else {
      user.send(embed)
      .then(res => {
      })
      .catch(res => {
        var err = new Error(JSON.stringify({"error": res, "discord": msg.author.id, "message": "chart"}));
        airbrake.notify(err)

        console.log("err: ", res)
      })
    }


  }).catch(err => {
    console.log(err)
    airbrake.notify(err)
    return sendErrorWebhook(webhook,"Something went wrong :worried:", "Rest assured, our team is working on it. Please try again later.")
  })

}

const helpWebhook = async (msg) => {
  msg.react('✅');

  const user = await client.users
  .fetch(msg.author.id)
  .catch((err) => console.log(err));
  const embed = new Discord.MessageEmbed()
  .setAuthor("Whop Bot Rentals", "https://whop.io/assets/whopbg.png", "https://whop.io")
  .setTitle("How To Use the Whop.io Discord Bot")
  .addField("Type `whop add-rental-feed` in your desired channel", "This command will create a Webhook in your server that will show a feed of all new Whop bot rental listings in real time. As soon as a bot rental listing goes live, it will be pushed to your server. This will create a feed in the channel where it was written in. You must have the `Manage Channels` permission to use this.")
  .addField("Type `whop add-transaction-feed` in your desired channel", "This command will create a webhook in your server that will show a feed of all Whop bot rental transactions in real time. This will create a feed in the channel where it was written in. You must have the `Manage Channels` permission to use this.")
  .setFooter("Whop.io","https://whop.io/assets/whopbg.png", "https://whop.io")
  .setColor("#ff5754")

  let channel = msg.channel
  if (channel) {
    channel.send(embed);
  } else {
    user.send(embed)
    .then(res => {
    })
    .catch(res => {
      var err = new Error(JSON.stringify({"error": res, "discord": msg.author.id, "message": "help"}));
      airbrake.notify(err)
      console.log("err: ", res)
    })
  }
}

const helpBotsWebhook = async (msg) => {
  msg.react('✅');

  const user = await client.users
  .fetch(msg.author.id)
  .catch((err) => console.log(err));
  const embed = new Discord.MessageEmbed()
  .setAuthor("Whop Bot Rentals", "https://whop.io/assets/whopbg.png", "https://whop.io")
  .setDescription("Use any of the commands below to access bot rental data. If you want to add a direct feed of new rentals and transactions to your server, type `whop setup` in your server")
  .setFooter("Whop.io","https://whop.io/assets/whopbg.png", "https://whop.io")
  .setColor("#ff5754")

  for (i in bots_full_names) {
    embed.addField(bots_full_names[i], `Type \`whop ${bots_full_names[i]}\``, true)
  }

  let channel = msg.channel
  if (channel) {
    channel.send(embed)
    .then(res => {
    })
    .catch(res => {
      airbrake.notify(res)
      console.log("err: ", res)
    })
  } else {
    user.send(embed)
    .then(res => {
    })
    .catch(res => {
      airbrake.notify(res)
      console.log("err: ", res)
    })
  }
}

const createErrorEmbed = (title, description) => {
  const embed = new Discord.MessageEmbed()
  .setTitle(title)
  .setDescription(description)
  .setFooter("Whop.io","https://whop.io/assets/whopbg.png", "https://whop.io")
  return embed
}




// Webhook name is the name of the webhook that will be shown
// Parameter name is either `transaction_webhook` or `new_listing_webhook`
const bindChannel = async (msg, webhook_name, parameter_name) => {
  let member = msg.member
  if (!member) {
    let errorMsg = createErrorEmbed("Uh oh", "This command must be written in the channel you want the feed to be in.")
    return msg.reply(errorMsg)
  }
  if (!member.hasPermission('MANAGE_CHANNELS')) {
    return msg.reply("You must have the `Manage Channels` permission to use this command.")
  }
  
  let channel = msg.channel
  let info = utils.getInfoFromMessage(msg)
  // Check if we already made the webhook
  let channel_webhooks = await channel.fetchWebhooks()
  let webhook = channel_webhooks.find(webhook => webhook.name == webhook_name)
  let err;
  
  if (!webhook) {
    // Webhook hasn't been made yet, gotta create a new one
    webhook =  await channel.createWebhook(webhook_name, {
      avatar: 'https://whop.io/assets/whopbg.png',
    }).catch(error => err = error)

    if (!webhook) {
      // No webhook exists. The .catch fired, show an error....
      console.log(err)
      airbrake.notify(err)
      return msg.reply("Something went wrong :worried: Rest assured, our team is working on it. Please try again later.")
    }
  }


  info[`${parameter_name}_url`] = webhook.url
  info[`${parameter_name}_channel_id`] = info["channel_id"]


  utils.sendRequest(config.domain, `/discord_servers/set_webhook`, info)
  .then(res => {
    if (parameter_name.includes("transaction")) return sendSuccessWebhook(webhook, "New Whop transactions will now be sent to this channel.")
    else if (parameter_name.includes("listing")) return sendSuccessWebhook(webhook, "New Whop listings will now be sent to this channel.")
    else msg.reply("Success!")
  }).catch(err => {
    console.log(err)
    airbrake.notify(err)
    return sendErrorWebhook(webhook,"Something went wrong :worried:", "Rest assured, our team is working on it. Please try again later.")
  })
}

const sendSuccessWebhook = (webhook, description) => {
  webhook.send(null, {
    embeds: [{
      "title": "Success!",
      "description": description,
      "color": 16736591,
      "footer": {
        "text": "Whop.io",
        "icon_url": "https://whop.io/assets/whopbg.png"
      },
      "thumbnail": {
        "url": "https://whop.io/assets/whopbg.png"
      }
    }]
  })
}


const sendErrorWebhook = (webhook, title, description) => {
  webhook.send(null, {
    embeds: [{
      "title": title,
      "description": description,
      "color": 16736591,
      "footer": {
        "text": "Whop.io",
        "icon_url": "https://whop.io/assets/whopbg.png"
      },
    
    }]
  })
}



// guildCreate
/* Emitted whenever the client joins a guild.
PARAMETER    TYPE         DESCRIPTION
guild        Guild        The created guild    */
client.on("guildCreate", function(guild, user){
});

const login = () => {
  client.login(config.tokenDiscord);
};

login();

const dmUser = async (req, res, next) => {

  let msg = req.body.content
  if (!msg || msg.length == 0 || msg.toString() == "200" || msg == 200) {
    return res.sendStatus(400)
  }
  // return
  try {
    // let user2 = client.fetchUser(req.body.discord_id)
    // console.log(client.users.))
    // params = JSON.parse(req.body)
    const user = await client.users
      .fetch(req.body.discord_account_id)
      .catch((err) => console.log(err));
    if (!user) {
      return res.sendStatus(400)
    }

    user.send(msg)
    .then(result => {
      return res.status(200).json({ message: "Message delivered" });
    })
    .catch(e => {
      return res.status(400).json({ message: e });
    })
  } catch (error) {
    // Unable to send message
    console.log(error);
    return res.sendStatus(400)
  }

}


const give_role = async (req, res) => {
  try {
  discordId = req.body.discord_account_id
  let status = 200
    const whopGuild = await client.guilds.fetch("802023806587502613").catch(e => status = 400);
    let role =  await whopGuild.roles.fetch("810979846984826930").catch(e => status = 400)
    const user = client.users.cache.get(discordId);
    const member = await whopGuild.members.fetch(discordId).catch(e => status = 400)
    if (status == 400) {
      return res.sendStatus(400)
    }
    if (req.body.add_role) {
      await member.roles
      .add(role)
      .then((res) => {
        console.log("Role added")
      })
      .catch((err) => {
        console.log(err);
        airbrake.notify(err)
        status = 400
      
      });
    } else {
      await member.roles
      .remove(role)
      .then((res) => {
        // Success
      })
      .catch((err) => {
        console.log(err);
        airbrake.notify(err)
        status = 400
      
      });
    }
    
    return res.sendStatus(status)

    // const user = client.users.cache.get(discordId);
    // const member = guild.member(user);
    roles = guild.roles.cache.array();
    console.log(guild)
    // let role =  await guild.roles.fetch("809243760340762684")
    console.log(role)
    return res.sendStatus(200)
    found = false;
    console.log("Tier: " + tier);
    console.log(member);
    console.log(roles)
    for (i in roles) {
      if (tier == roles[i].name) {
        console.log("Found");
        console.log(roles[i]);
        found = true;

        await member.roles
          .add(roles[i])
          .then((res) => {
            // Success
            console.log("Done");
            return res.sendStatus(200);
          })
          .catch((err) => {
            console.log("error");
            return res.sendStatus(400)
          
          });

        break;
      }
    }
  
  } catch (error) {
    console.log(error);
    airbrake.notify(error)
    // return sendAdminWebhook(
    //   `@sharkey\nThere was an error adding the ${tier} role to user <@${discordId}>. \n\nError Message:\n${error}`
    // );
  }
};

const give_prime_role = async (req, res) => {
  try {
  discordId = req.body.discord_account_id
  let status = 200
    const whopGuild = await client.guilds.fetch("802023806587502613").catch(e => status = 400);
    let role =  await whopGuild.roles.fetch("847520772196007987").catch(e => status = 400)
    const user = client.users.cache.get(discordId);
    const member = await whopGuild.members.fetch(discordId).catch(e => status = 400)
    if (status == 400) {
      return res.sendStatus(400)
    }
    if (req.body.add_role) {
      await member.roles
      .add(role)
      .then((res) => {
        console.log("Role added")
      })
      .catch((err) => {
        console.log(err);
        airbrake.notify(err)
        status = 400
      
      });
    } else {
      await member.roles
      .remove(role)
      .then((res) => {
        // Success
      })
      .catch((err) => {
        console.log(err);
        airbrake.notify(err)
        status = 400
      
      });
    }
    
    return res.sendStatus(status)

    // const user = client.users.cache.get(discordId);
    // const member = guild.member(user);
    roles = guild.roles.cache.array();
    console.log(guild)
    // let role =  await guild.roles.fetch("809243760340762684")
    console.log(role)
    return res.sendStatus(200)
    found = false;
    console.log("Tier: " + tier);
    console.log(member);
    console.log(roles)
    for (i in roles) {
      if (tier == roles[i].name) {
        console.log("Found");
        console.log(roles[i]);
        found = true;

        await member.roles
          .add(roles[i])
          .then((res) => {
            // Success
            console.log("Done");
            return res.sendStatus(200);
          })
          .catch((err) => {
            console.log("error");
            return res.sendStatus(400)
          
          });

        break;
      }
    }
  
  } catch (error) {
    console.log(error);
    airbrake.notify(error)
    // return sendAdminWebhook(
    //   `@sharkey\nThere was an error adding the ${tier} role to user <@${discordId}>. \n\nError Message:\n${error}`
    // );
  }
};

const give_proxies_role = async (req, res) => {
  try {
  discordId = req.body.discord_account_id
  let status = 200
    const whopGuild = await client.guilds.fetch("802023806587502613").catch(e => status = 400);
    let role =  await whopGuild.roles.fetch("851957765585108995").catch(e => status = 400)
    const user = client.users.cache.get(discordId);
    const member = await whopGuild.members.fetch(discordId).catch(e => status = 400)
    if (status == 400) {
      return res.sendStatus(400)
    }
    if (req.body.add_role) {
      await member.roles
      .add(role)
      .then((res) => {
        console.log("Role added")
      })
      .catch((err) => {
        console.log(err);
        airbrake.notify(err)
        status = 400
      
      });
    } else {
      await member.roles
      .remove(role)
      .then((res) => {
        // Success
      })
      .catch((err) => {
        console.log(err);
        airbrake.notify(err)
        status = 400
      
      });
    }
    
    return res.sendStatus(status)

    // const user = client.users.cache.get(discordId);
    // const member = guild.member(user);
    roles = guild.roles.cache.array();
    console.log(guild)
    // let role =  await guild.roles.fetch("809243760340762684")
    console.log(role)
    return res.sendStatus(200)
    found = false;
    console.log("Tier: " + tier);
    console.log(member);
    console.log(roles)
    for (i in roles) {
      if (tier == roles[i].name) {
        console.log("Found");
        console.log(roles[i]);
        found = true;

        await member.roles
          .add(roles[i])
          .then((res) => {
            // Success
            console.log("Done");
            return res.sendStatus(200);
          })
          .catch((err) => {
            console.log("error");
            return res.sendStatus(400)
          
          });

        break;
      }
    }
  
  } catch (error) {
    console.log(error);
    airbrake.notify(error)
    // return sendAdminWebhook(
    //   `@sharkey\nThere was an error adding the ${tier} role to user <@${discordId}>. \n\nError Message:\n${error}`
    // );
  }
};

/*
params = 
{
    "bot_name" : "Cybersole",
    "order_number" : "9999",
    "buyer_id" : "804808229732352030",
    "middleman_id" : "666778130542886934",
    "seller_id": "633130757996544012"
}
*/

const createFirstSaleChannel = async (req, res, next) => {
  let channel_name = req.body.bot_name + "-" + req.body.bot_id

  const whopGuild = await client.guilds.fetch("869380404887560203").catch(e => console.log(e));
  const category = await client.channels.fetch("892465656459591710").catch(e => console.log(e));

  let channels = whopGuild.channels
  const owner_id = req.body.owner_id

  let buyer = await whopGuild.members.fetch(owner_id).catch(err => console.log(err))
  if (!buyer) {
    // buyer = await client.users.fetch(owner_id).catch(err => console.log(err))
    // if (buyer) {
    //   buyer.send("https://discord.gg/jssX79udPv")
    // }
    return res.json({"error": "buyer missing"}).status(400)
  }


  channels.create(channel_name, {permissionOverwrites: [
    {
      id: whopGuild.id,
      deny: ['VIEW_CHANNEL'],
    },
    {
      id: buyer.id,
      allow: ['VIEW_CHANNEL'],
    }, 
    {
      id: "912503664747360316",
      allow: ['VIEW_CHANNEL'],
    },
    {
      id: "870443450892435568",
      allow: ['VIEW_CHANNEL'],
    }
  ]})
  .then(channel => {
    return res.sendStatus(200)
  })
  .catch(err => {
    console.log(err)
    airbrake.notify(err)
    return res.sendStatus(400)
  })


}

const createTransactionChannel = async (req, res, next) => {
  let channel_name_buyer = req.body.order_number + " - Buyer - " + req.body.bot_name 
  let channel_name_seller = req.body.order_number + " - Seller - " + req.body.bot_name 

  const whopGuild = await client.guilds.fetch("802023806587502613").catch(e => console.log(e));
  const category = await client.channels.fetch("816717649639112705").catch(e => console.log(e));

  let channels = whopGuild.channels
  const buyer_id = req.body.buyer_id
  const middleman_id = req.body.middleman_id
  const seller_id = req.body.seller_id

  let buyer = await whopGuild.members.fetch(buyer_id).catch(err => console.log(err))
  if (!buyer) {
    buyer = await client.users.fetch(buyer_id).catch(err => console.log(err))
    if (buyer) {
      buyer.send("https://discord.gg/ZTxHxtuXTM")
    }
    return res.json({"error": "buyer missing"}).status(400)
  }

  let middleman = await whopGuild.members.fetch(middleman_id).catch(err => console.log(err))
  if (!middleman) {
    middleman = await client.users.fetch(middleman_id).catch(err => console.log(err))
    if (middleman) {
      middleman.send("https://discord.gg/ZTxHxtuXTM")
    }
    return res.json({"error": "middleman missing"}).status(400)
  }


  let seller = await whopGuild.members.fetch(seller_id).catch(err => console.log(err))
  if (!seller) {
    seller = await client.users.fetch(seller_id).catch(err => console.log(err))
    if (seller) {
      seller.send("https://discord.gg/ZTxHxtuXTM")
    }
    return res.json({"error": "seller missing"}).status(400)
  }


  // Buyer & Middleman
  const description = `Transaction #${req.body.order_number} for ${req.body.bot_name}. Buyer: ${buyer.user.username}, Middleman: ${middleman.user.username}`
  let channel_ids = {}
  channels.create(channel_name_buyer, {parent: category, topic: description, permissionOverwrites: [
    {
      id: whopGuild.id,
      deny: ['VIEW_CHANNEL'],
    },
    {
      id: buyer.id,
      allow: ['VIEW_CHANNEL'],
    },
    {
      id: middleman.id,
      allow: ['VIEW_CHANNEL'],
    }
  ]})
  .then(channel => {
    channel_ids["buyer"] = channel.id
    // let msg = `<@${buyer_id}> <@${middleman_id}> Congrats! You're soon to be the proud new owner of a ${req.body.bot_name} copy! The middleman process is now underway. As soon as your seller binds their key to the Whop dashboard, you will be prompted to complete your payment for the bot. If your payment is not processed within 24 hours of the key being bound to Whop, your sale will be automatically canceled.\n\nOnce your payment process, we will send over all the key ownership information, giving you access to bind it to your own account. \n\nIf you have any questions about the middleman processes, you can learn more here: (http://docs.whop.io/).`
    let msg = buildBuyerChannelEmbed(buyer_id, middleman_id, req.body.bot_name)
    channel.send(msg).then((result) => { 
      console.log(result)
      result.react("✂️")
      channel.send("@here")
    });
    console.log("created channel")
  })
  .catch(err => {
    console.log(err)
    airbrake.notify(err)
    return res.sendStatus(400)
  })

  // Seller and middleman
  channels.create(channel_name_seller, {parent: category, topic: description, permissionOverwrites: [
    {
      id: whopGuild.id,
      deny: ['VIEW_CHANNEL'],
    },
    {
      id: seller.id,
      allow: ['VIEW_CHANNEL'],
    },
    {
      id: middleman.id,
      allow: ['VIEW_CHANNEL'],
    }
  ]})
  .then(channel => {
    channel_ids["seller"] = channel.id
    // let msg = `<@${seller_id}> <@${middleman_id}> Congrats! You sold your ${req.body.bot_name} copy on Whop. The middleman process is now underway. The next step is binding your key to your Whop middleman. We will use this chat to facilitate that process. As soon as you are ready please send the key over and tag the middleman in this chat. DO NOT UNBIND THE KEY YET just send over the license key. \n\nWithin 24 hours of your key being bound to the Whop dashboard, we will collect the funds from your buyer.\n\nIf you have any questions about the middleman process, you can learn more here: (http://docs.whop.io/).`
    let msg = buildSellerChannelEmbed(seller_id, middleman_id, req.body.bot_name)

    channel.send(msg).then((result) => { 
      result.react("✂️")
      channel.send("@here")
  

    });
    return res.json(channel_ids)
  })
  .catch(err => {
    console.log(err)
    airbrake.notify(err)
    return res.sendStatus(400)
  })
}

const checkWhopxServerStatus = async (req, res, next) => {
  console.log("checking")
  console.log(req.body.id)
  const whopGuild = await client.guilds.fetch("817198536802041916").catch(e => console.log(e));
  let user = await whopGuild.members.fetch(req.body.id).catch(err => console.log(err))
  if (!user) {
    return res.sendStatus(400)
  } else {
    return res.sendStatus(200)
  }
}
// params = {id: "<discord_id>"}
const checkServerStatus = async (req, res, next) => {
  console.log("checking")
  const whopGuild = await client.guilds.fetch("802023806587502613").catch(e => console.log(e));
  let user = await whopGuild.members.fetch(req.body.id).catch(err => console.log(err))
  if (!user) {
    // user = await client.users.fetch(req.body.id).catch(err => console.log(err))
    // if (user) {
    //   user.send("https://discord.gg/ZTxHxtuXTM")
    //   .catch(err => {
    //     console.log(err)
    //   })
    // }
    return res.sendStatus(400)
  } else {
    return res.sendStatus(200)
  }
}

// Send a message to a channel
const sendToChannel = (message, user) => { 
  return new Promise(function (resolve, reject) {
  bot.channels
  .fetch(channel_id)
  .then((channel) => { 
  channel.send(message).then((msg) => { 
  return resolve(msg);
  });
  }) 
  .catch(console.error);
  });
};

const isAdminOrMod = async (userId) => {
  return new Promise(async function (resolve, reject) {
    const guild = await client.guilds.fetch("802023806587502613").catch(e => status = 400);
    await guild.members
      .fetch(userId)
      .then(async (user) => {
        for (i in user["_roles"]) {
          await guild.roles.fetch(user["_roles"][i]).then((role) => {
            if (role.name == "Admin" || role.name == "Mod") {
              return resolve(true)
            }
          })
        }
        return resolve(false)
      })
  })
}

const rentalStarted = async (req, res, next) => {

  let status = 200
  let bot_name = req.body.bot_name.replace(/[^A-Z0-9]/ig, "")
  let discord_account_id = req.body.discord_account_id
  const whopGuild = await client.guilds.fetch("802023806587502613").catch(e => status = 400);
  // let user = await whopGuild.members.fetch(discord_account_id).catch(console.error)
  let user = await client.users.fetch(discord_account_id).catch(console.error)
  console.log(user)
  console.log(discord_account_id)
  if (user == undefined) {
    // Member not in whop
    console.log("not in whop -- can't do anything")
    return res.sendStatus(400)
  }

  // Zonos Shit
  if (bot_name == "ZonosLabs") {
    console.log("zonos ")
    // const zonosGuild = await client.guilds.fetch("638101979318714398").catch(e => status = 400);
    const welcomeChannel = await client.channels.fetch("638105985298202625").catch(e => console.log(e));
    if (welcomeChannel) {
      let invite = await welcomeChannel.createInvite({
        maxUses: 1,
        maxAge: 86400,
        unique: true,
        reason: "Whop bot"
      }).catch(console.error);

      if (invite) {
        let url = `https://discord.gg/${invite.code}`
        let message = `Hi! Your recent rental of **Zonos Labs** also comes with private Discord access!\n\nJoin their server here: ${url}`
        await user.send(message)
      }
    }
  } else if (bot_name == "Koi") {
    console.log("Koi")
    // const zonosGuild = await client.guilds.fetch("638101979318714398").catch(e => status = 400);
    const welcomeChannel = await client.channels.fetch("735253597180264468").catch(e => console.log(e));
    if (welcomeChannel) {
      let invite = await welcomeChannel.createInvite({
        maxUses: 1,
        maxAge: 86400,
        unique: true,
        reason: "Whop bot"
      }).catch(console.error);

      if (invite) {
        let url = `https://discord.gg/${invite.code}`
        let message = `Hi! Your recent rental of **Koi** also comes with private Discord access!\n\nJoin their server here: ${url}`
        await user.send(message)
      }
    }

  }



  const guild = whopGuild
  user = await guild.members.fetch(discord_account_id).catch(console.error)
  if (user == undefined || user.roles == undefined) {
    // Member not in whop
    console.log("not in whopx")
    return res.sendStatus(400)
  }

  // var randomColor = Math.floor(Math.random()*16777215).toString(16);
  // let roles = guild.roles.cache.array().filter(role => role.name === bot_name);
  // let role;

  // if (roles.length == 0) {
  //   // Create the role
  //   role = await guild.roles.create({
  //     data: {
  //       name: bot_name,
  //       color: randomColor
  //     }
  //   });
  // } else {
  //   role = roles[0]
  // }

  // let active_rental_role = guild.roles.resolve("842863005980950538")
  // await user.roles
  // .add(active_rental_role)
  // .then((res) => {
  //   console.log(`${bot_name} role added to ${user.id}`)
  // })
  // .catch((err) => {
  //   console.log(err);
  //   airbrake.notify(err)
  //   status = 400
  // });

  // await user.roles
  // .add(active_rental_role)
  // .then((res) => {
  //   console.log(`${bot_name} role added to ${user.id}`)
  // })
  // .catch((err) => {
  //   console.log(err);
  //   airbrake.notify(err)
  //   status = 400
  // });

  res.sendStatus(status)
}

const rentalEnded = async (req, res, next) => {
  let status = 200
  let bot_name = req.body.bot_name.replace(/[^A-Z0-9]/ig, "")
  let discord_account_id = req.body.discord_account_id
 

  if (bot_name == "ZonosLabs") {
    const zonosGuild = await client.guilds.fetch("638101979318714398").catch(e => status = 400);
    if (zonosGuild) {
      let user = await zonosGuild.members.fetch(discord_account_id).catch(console.error)
      if (user) {
        await user.kick("rental ended")
        return res.send(500)
      }
    }
  }

  if (bot_name == "Koi") {
    const koiGuild = await client.guilds.fetch("735106117218074664").catch(e => status = 400);
    if (koiGuild) {
      let user = await koiGuild.members.fetch(discord_account_id).catch(console.error)
      if (user) {
        await user.kick("rental ended")
        return res.send(500)
      }
    }
  }

  const guild = await client.guilds.fetch("817198536802041916").catch(e => status = 400);
  user = await client.users.fetch(discord_account_id).catch(console.error)
  if (user == undefined || user.roles == undefined) {
    // Member not in whop
    console.log("not in whopx")
    return res.sendStatus(400)
  }


  let roles = guild.roles.cache.array().filter(role => role.name === bot_name);
  let role;

  if (roles.length == 0) {
    console.log(`Tried to remove a role but couldn't find it for ${bot_name} for ${discord_account_id}`)
    airbrake.notify(`Tried to remove a role but couldn't find it for ${bot_name} for ${discord_account_id}`)
    return res.sendStatus(400)
  } else {
    role = roles[0]
  }

  await user.roles
  .remove(role)
  .then((res) => {
    console.log(`${bot_name} role removed to ${user.id}`)
  })
  .catch((err) => {
    console.log(err);
    airbrake.notify(err)
    status = 400
  
  });

  let active_rental_role = guild.roles.resolve("836642646209462374")
  await user.roles
  .remove(active_rental_role)
  .then((res) => {
    console.log(`${bot_name} role removed to ${user.id}`)
  })
  .catch((err) => {
    console.log(err);
    airbrake.notify(err)
    status = 400
  
  });

  res.sendStatus(status)
}

const cleanUpZonos = async () => {
  const zonosGuild = await client.guilds.fetch("638101979318714398").catch(e => status = 400);
  let members = await zonosGuild.members.fetch()
  console.log("fetching")
  let rentalRoleId = "836694413669433402"
  let renters = members.array().filter(member => member._roles.includes("836694413669433402"));
  console.log(renters.length)
  console.log("zonos")
  for (i in renters) {
    let user = renter[i]
    let active_rental = await checkForActiveZonosRental(user.id)
    if (active_rental == false) {
      await user.kick("No longer has an active rental on Whop")
    }
  }
}

const cleanUpKoi = async () => {
  const koiGuild = await client.guilds.fetch("735106117218074664").catch(e => status = 400);
  let members = await koiGuild.members.fetch()
  console.log("fetching")
  let rentalRoleId = "857686348802293760"
  let renters = members.array().filter(member => member._roles.includes(rentalRoleId));
  console.log(renters.length)
  console.log("koi")
  for (i in renters) {
    let user = renters[i]
    let active_rental = await checkForActiveKoiRental(user.id)
    console.log("active rental: ", active_rental)
    if (active_rental == false) {
      await user.kick("No longer has an active rental on Whop")
    }
  }
}

const checkForActiveZonosRental = (discord_account_id) => {
  return new Promise(async function (resolve, reject) {
    utils.sendRequest(config.domain, `/discord/zonos_check`, {discord_account_id})
    .then(res => {
      return resolve(res["valid"])
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
      return reject()
    })
  })
}

const checkForActiveHayhaRental = (discord_account_id) => {
  return new Promise(async function (resolve, reject) {
    utils.sendRequest(config.domain, `/discord/hayha_check`, {discord_account_id})
    .then(res => {
      return resolve(res["valid"])
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
      return reject()
    })
  })
}

const checkForActiveKoiRental = (discord_account_id) => {
  return new Promise(async function (resolve, reject) {
    utils.sendRequest(config.domain, `/discord/koi_check`, {discord_account_id})
    .then(res => {
      return resolve(res["valid"])
    }).catch(err => {
      console.log(err)
      airbrake.notify(err)
      return reject()
    })
  })
}

const createBotChannelCore = async (bot_name, image_url) => {
  return new Promise(async function (resolve, reject) {
    let formatted_bot_name = bot_name.replace(/[^A-Z0-9]/ig, "")
    const guild = await client.guilds.fetch("817198536802041916").catch(err => airbrake.notify(err))
    if (!guild) return resolve(400)
  
    var randomColor = Math.floor(Math.random()*16777215).toString(16);
    let roles = guild.roles.cache.array().filter(role => role.name === formatted_bot_name);
    let role;
  
    if (roles.length == 0) {
      // Create the role
      role = await guild.roles.create({
        data: {
          name: formatted_bot_name,
          color: randomColor
        }
      });
    } else {
      role = roles[0]
    }
  
    let channels = guild.channels
    let everyone_id = "817198536802041916"
    let cyber_id = role.id
    let staff_id = "817204675316154378"
    let support_id = "825625772328484884"
    let premium_id = "817215648001818656"
  
    let perm_code = 68608
    let perm_deny = 806354961
  
  
    // Give staff, support, preimiuum and name of bot their role
    let category = await channels.create(`${bot_name} Support`, {type: "category" , permissionOverwrites: [
      {
        id: everyone_id,
        type: 0,
        allow: 0,
        deny: 68608
      },
      {
        id: premium_id,
        type: 0,
        allow: 0,
        deny: 68608
      },
      {
        id: cyber_id,
        type: 0,
        deny: perm_deny,
        allow: perm_code
      },
      {
        id: staff_id,
        type: 0,
        deny: perm_deny,
        allow: perm_code
      },
      {
        id: support_id,
        type: 0,
        deny: perm_deny,
        allow: perm_code
      }
    ]})
    .catch(err => {
      airbrake.notify(err)
      return resolve(400)
    })
  
    if (undefined == category)  return resolve(400)

  
    // Create announcements
    let see_and_no_send_allow = 66560
    let see_and_no_send_deny = 2048
  
    // For staff
    let see_and_send_allow = 68608
    let see_and_send_deny = 0
    
  
    await channels.create(`${bot_name}-announcements`, {parent: category, permissionOverwrites: [
      {
        id: everyone_id,
        type: 0,
        allow: 0,
        deny: 68608
      },
      {
        id: staff_id,
        type: 0,
        deny: see_and_send_deny,
        allow: see_and_send_allow
      },
      {
        id: cyber_id,
        type: 0,
        deny: see_and_no_send_deny,
        allow: see_and_no_send_allow
      },
    
      {
        id: support_id,
        type: 0,
        deny: see_and_no_send_deny,
        allow: see_and_no_send_allow
      }
    ]})
    .then(result => {
      console.log("success announce")
    })
    .catch(err => {
      airbrake.notify(err)
      return resolve(400)
    })
  
    await channels.create(`${bot_name}-chat`, {parent: category})
    .then(result => {
      console.log("success announce")
    })
    .catch(err => {
      airbrake.notify(err)
      return resolve(400)
    })
  
  
    await channels.create(`${bot_name}-support`, {parent: category})
    .then(result => {
      console.log("success announce")
    })
    .catch(err => {
      airbrake.notify(err)
      return resolve(400)
    })
  
    let emoji = await guild.emojis.create(image_url, formatted_bot_name).catch(console.error);
  
    if (emoji == undefined) {
      airbrake.notify("Couldn't create emoji role for " + bot_name)
      return resolve(400)
    }
  
    let bot_support_roles_channel = guild.channels.resolve("835217981708697641")
    var roles_message_id = "836685560570839050"
  
    bot_support_roles_channel.messages.fetch(roles_message_id)
    .then(message =>  {
      current_content = message.content
      new_message = current_content + "\n" + `React with ${emoji} for ${bot_name} support`
  
      message.edit(new_message)
      .then(e => {
        console.log("edited")
      })
      .catch(err => {
        airbrake.notify(err)
        return resolve(400)
      })
  
      message.react(emoji)
      .catch(err => {
        airbrake.notify(err)
        return resolve(400)
      })
    })
    .catch(err => {
      airbrake.notify(err)
      return resolve(400)
    })
    return resolve(200)
  })
}

const createBotChannel = async (req, res, next) => {
  createBotChannelCore(req.body.bot_name, req.body.image_url)
  .then(code => {
    return res.send(code)
  })
}

const fixMessage = async (req, res, next) => {
  let bot_name = req.body.bot_name
  let formatted_bot_name = bot_name.replace(/[^A-Z0-9]/ig, "")
  const guild = await client.guilds.fetch("817198536802041916")
  .catch(err => {
    airbrake.notify(err)
    return res.send(400)
  })


  // var randomColor = Math.floor(Math.random()*16777215).toString(16);
  // let roles = guild.roles.cache.array().filter(role => role.name === formatted_bot_name);
  // let role;

  // if (roles.length == 0) {
  //   // Create the role
  //   role = await guild.roles.create({
  //     data: {
  //       name: formatted_bot_name,
  //       color: randomColor
  //     }
  //   });
  // } else {
  //   role = roles[0]
  // }





  // console.log(guild)
  // let channels = guild.channels
  // let everyone_id = "817198536802041916"
  // let cyber_id = role.id
  // let staff_id = "817204675316154378"
  // let support_id = "825625772328484884"
  // let premium_id = "817215648001818656"

  // let perm_code = 68608
  // let perm_deny = 806354961

  // console.log(bot_name)

  // // Give staff, support, preimiuum and name of bot their role
  // let category = await channels.create(`${bot_name} Support`, {type: "category" , permissionOverwrites: [
  //   {
  //     id: everyone_id,
  //     type: 0,
  //     allow: 0,
  //     deny: 68608
  //   },
  //   {
  //     id: premium_id,
  //     type: 0,
  //     allow: 0,
  //     deny: 68608
  //   },
  //   {
  //     id: cyber_id,
  //     type: 0,
  //     deny: perm_deny,
  //     allow: perm_code
  //   },
  //   {
  //     id: staff_id,
  //     type: 0,
  //     deny: perm_deny,
  //     allow: perm_code
  //   },
  //   {
  //     id: support_id,
  //     type: 0,
  //     deny: perm_deny,
  //     allow: perm_code
  //   }
  // ]})
  // .catch(err => {
  //   airbrake.notify(err)
  //   return res.send(400)
  // })

  // if (undefined == category) return res.send(400)


  // // Create announcements
  // let see_and_no_send_allow = 66560
  // let see_and_no_send_deny = 2048

  // // For staff
  // let see_and_send_allow = 68608
  // let see_and_send_deny = 0
  

  // await channels.create(`${bot_name}-announcements`, {parent: category, permissionOverwrites: [
  //   {
  //     id: everyone_id,
  //     type: 0,
  //     allow: 0,
  //     deny: 68608
  //   },
  //   {
  //     id: staff_id,
  //     type: 0,
  //     deny: see_and_send_deny,
  //     allow: see_and_send_allow
  //   },
  //   {
  //     id: cyber_id,
  //     type: 0,
  //     deny: see_and_no_send_deny,
  //     allow: see_and_no_send_allow
  //   },
  
  //   {
  //     id: support_id,
  //     type: 0,
  //     deny: see_and_no_send_deny,
  //     allow: see_and_no_send_allow
  //   }
  // ]})
  // .then(result => {
  //   console.log("success announce")
  // })
  // .catch(err => {
  //   airbrake.notify(err)
  //   return res.send(400)
  // })

  // await channels.create(`${bot_name}-chat`, {parent: category})
  // .then(result => {
  //   console.log("success announce")
  // })
  // .catch(err => {
  //   airbrake.notify(err)
  //   return res.send(400)
  // })


  // await channels.create(`${bot_name}-support`, {parent: category})
  // .then(result => {
  //   console.log("success announce")
  // })
  // .catch(err => {
  //   airbrake.notify(err)
  //   return res.send(400)
  // })

  let emoji = await guild.emojis.cache.array().filter(role => role.name === formatted_bot_name);

  if (emoji.length == 0) {
    emoji = await guild.emojis.create("https://whop.io/assets/whopbg.png", "whop").catch(console.error);
  } else {
    emoji = emoji[0]
  }

  let bot_support_roles_channel = guild.channels.resolve("835217981708697641")
  var roles_message_id = "836685560570839050"

  let message = await bot_support_roles_channel.messages.fetch(roles_message_id)
  current_content = message.content
  new_message = current_content + `\nReact with ${emoji} for ${bot_name} support`
  await message.edit(new_message)
  await message.react(emoji)
  // .then(async message =>  {
  //   current_content = message.content
  //   new_message = current_content + "\n\n" + `React with ${emoji} for ${bot_name} support`

  //   await message.edit(new_message)
  //   .then(async e => {
  //     console.log("edited")
  //     await message.react(emoji)
  //     .then(e => {
  //       console.log("reacted")
  //     })
  //     .catch(err => {
  //       console.log(err)
  //       airbrake.notify(err)
  //       return res.send(400)
  //     })
  //   })
  //   .catch(err => {
  //     airbrake.notify(err)
  //     return res.send(400)
  //   })

  // })
  // .catch(err => {
  //   console.log(err)
  //   airbrake.notify(err)
  //   return res.send(400)
  // })








  return res.send(200)

  // Annoucements no one can write

  // Chat & support same


}

  

module.exports = {
  dmUser,
  give_role,
  createTransactionChannel,
  checkServerStatus,
  checkWhopxServerStatus,
  rentalStarted,
  rentalEnded,
  createBotChannel,
  fixMessage,
  give_prime_role,
  give_proxies_role,
  createFirstSaleChannel
}


